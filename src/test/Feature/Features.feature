Feature:Confirm whether a visa is required to visit the UK

  Background: Common step for every Scenario
    Given I visit Uk visa Web site


  Scenario:A Japanese visiting to study for 6 months
    And I Provide a Nationality of Japan
    And I select the reason "Study"
    And I state i am intending to Stay for more than 6 months
    When I submit the form
    Then I will be informed "You’ll need a visa to study in the UK"


  Scenario: Check if visa required for Tourism
    And I Provide a Nationality of Japan
    And I select the reason Tourism
    When I submit the form
    Then I will be informed That "You won’t need a visa to come to the UK"


  Scenario: Verification of Russian citizen visa requirements
    And I Provide a Nationality of Russia
    And I select the reason Tourism
    And I state i am not travelling or visiting a partner or family
    When I submit the form
    Then I will be informed that "You’ll need a visa to come to the UK"