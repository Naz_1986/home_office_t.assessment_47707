import commonFunctions.CommonFunctions;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
    CommonFunctions cmnFunction= new CommonFunctions();


    @Before
    public void beforeTest()
    {
        cmnFunction.openBrowser();


    }

    @After
    public void afterTest()
    {
        cmnFunction.closeBrowser();
    }
}
