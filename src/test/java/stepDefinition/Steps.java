package stepDefinition;

import commonFunctions.CommonFunctions;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import pageObjects.PageObjects;

public class Steps extends CommonFunctions {
    PageObjects pageObj= new PageObjects(driver);


    @Given("^I visit Uk visa Web site$")
    public void i_visit_Uk_visa_Web_site() throws InterruptedException {
        driver.get("https://www.gov.uk/check-uk-visa/");
        pageObj.setStartNow();
        pageObj.setHideMessage();

        Thread.sleep(3000);
    }

    @Given("^I Provide a Nationality of Japan$")
    public void i_Provide_a_Nationality_of_Japan() throws InterruptedException {
        pageObj.setDropDown("Japan");
        pageObj.setNextSBtn();
        Thread.sleep(3000);
    }

    @Given("^I select the reason \"([^\"]*)\"$")
    public void i_select_the_reason(String arg1) throws InterruptedException {
        pageObj.setCheckBox();
        pageObj.setNextSBtn1();
        Thread.sleep(3000);
    }

    @Given("^I state i am intending to Stay for more than (\\d+) months$")
    public void i_state_i_am_intending_to_Stay_for_more_than_months(int arg1) throws InterruptedException {
        pageObj.setCheckBox1();
        pageObj.setNextSBtn2();
        Thread.sleep(3000);
    }

    @When("^I submit the form$")
    public void i_submit_the_form() throws InterruptedException {
        Thread.sleep(3000);

    }

    @Then("^I will be informed \"([^\"]*)\"$")
    public void i_will_be_informed(String expectedMessage) throws InterruptedException {
        String actualMessage=driver.findElement(By.xpath("//*[@id=\"result-info\"]/div[2]/h2")).getText();
        Assert.assertEquals(expectedMessage,actualMessage);
        Thread.sleep(3000);
    }

    //Japan Tourism visa steps

    @Given("^I select the reason Tourism$")
    public void i_select_the_reason_Tourism() throws InterruptedException {
        pageObj.setCheckBoxTourism();
        pageObj.setNextSBtn2();

    }

    @Then("^I will be informed That \"([^\"]*)\"$")
    public void i_will_be_informed_That(String expectedMessage1) throws InterruptedException{
        String actualMessage1=driver.findElement(By.xpath("//*[@id=\"result-info\"]/div[2]/h2")).getText();
        Assert.assertEquals(expectedMessage1,actualMessage1);
        Thread.sleep(3000);
    }
    // Russian and Tourism Steps Definitions

    @Given("^I Provide a Nationality of Russia$")
    public void i_Provide_a_Nationality_of_Russia() throws InterruptedException {
        pageObj.setDropDownRussia("Russia");
        pageObj.setNextSBtn1();
        Thread.sleep(3000);
    }

    @Given("^I state i am not travelling or visiting a partner or family$")
    public void i_state_i_am_not_travelling_or_visiting_a_partner_or_family() throws InterruptedException {
        pageObj.setCheckBRussia();
        pageObj.setNextSBtn1();
        Thread.sleep(3000);
    }

    @Then("^I will be informed that \"([^\"]*)\"$")
    public void i_will_be_informed_that(String expectedMessage2) throws InterruptedException{
        String actualMessage2=driver.findElement(By.xpath("//*[@id=\"result-info\"]/div[2]/h2")).getText();
        Assert.assertEquals(expectedMessage2,actualMessage2);
        Thread.sleep(3000);

    }

}
