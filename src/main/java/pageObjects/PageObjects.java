package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class PageObjects {
    //WebElements for Japan Visa Requirements

    @FindBy(xpath = "//*[@id=\"global-bar\"]/div[2]/div[2]/a")
    WebElement HideMessage;
    //@FindBy(xpath = "")
    //WebElement acceptCookies;

    @FindBy(xpath = "//*[@id=\"get-started\"]/a")
    WebElement StartNow;

    @FindBy(id = "response")
    WebElement DropDown;

    @FindBy(xpath = "//*[@id=\"current-question\"]/button")
    WebElement NextSBtn;

    @FindBy(id = "response-2")
    WebElement CheckBox;

    @FindBy(xpath = "//*[@id=\"current-question\"]/button")
    WebElement NextSBtn1;

    @FindBy(id = "response-1")
    WebElement CheckBox1;

    @FindBy(xpath = "//*[@id=\"current-question\"]/button")
    WebElement NextSBtn2;

    //Tourism visa Elements

    @FindBy(id = "response-0")
    WebElement CheckBoxTourism;

    //Russian and Tourism

    @FindBy(id = "response")
    WebElement DropDownRussia;

    @FindBy(id = "response-1")
    WebElement CheckBRussia;


    //Constructor...........................
    public PageObjects(WebDriver driver)
    {
        PageFactory.initElements(driver,this);
    }

    //Methods...............................

    public void setHideMessage()
    {
        HideMessage.click();
    }

    public void setStartNow()
    {
        StartNow.click();
    }



    public void setDropDown(String dropDown)
    {
        Select country=new Select(DropDown);
        country.selectByVisibleText(dropDown);

    }
    public void setNextSBtn()
    {
        NextSBtn.click();
    }
    public void setCheckBox()
    {
        CheckBox.click();
    }
    public void setNextSBtn1()
    {
        NextSBtn1.click();
    }

    public void setCheckBox1()
    {
        CheckBox1.click();
    }
    public void setNextSBtn2()
    {
        NextSBtn2.click();
    }

    public void setCheckBoxTourism()
    {
        CheckBoxTourism.click();
    }

    //Russia and tourism

    public void setDropDownRussia(String dropDown1)
    {
        Select country1=new Select(DropDown);
        country1.selectByVisibleText(dropDown1);

    }
    public void setCheckBRussia()
    {
        CheckBRussia.click();
    }

}
